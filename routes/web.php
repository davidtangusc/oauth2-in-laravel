<?php

use App\TwitterAPI;

Route::get('/', function () {
    return view('index');
});

Route::get('/login', 'LoginController@redirectToProvider');
Route::get('/login/github/callback', 'LoginController@handleProviderCallback');
Route::get('/logout', function() {
    Auth::logout();
    return redirect('/');
});

Route::get('/account', function() {
    return view('account');
})->middleware('authenticated');

Route::get('/user', function() {
    return Auth::user();
});

Route::get('/tweets', function() {
    // https://dev.twitter.com/oauth/application-only
    $twitterAPI = new TwitterAPI([
        'consumer_key' => env('TWITTER_CONSUMER_KEY'),
	    'consumer_secret' => env('TWITTER_CONSUMER_SECRET')
    ]);

    return $twitterAPI->get('statuses/user_timeline', [
        'screen_name' => 'skaterdav85',
        'count' => 10,
        'exclude_replies' => true
    ]);
});