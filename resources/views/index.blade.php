@if (Auth::check())
  <h1>Welcome, {{Auth::user()->username}}</h1>
  <img src="{{Auth::user()->avatar}}" style="width: 100px;">
  <a href="/account">My Account</a>
  <a href="/logout">Logout</a>
@else
  <a href="/login">Login with Github</a>
@endif