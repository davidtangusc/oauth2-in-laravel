<?php

namespace App\Http\Controllers;

use Socialite;
use App\User;
use Auth;

class LoginController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    public function handleProviderCallback()
    {
        // i think this takes the code and gets an access token and uses that
        // access token to get the user
        $user = Socialite::driver('github')->user();
        $user = User::firstOrCreate([
            'username' => $user->getNickname(),
            'email' => $user->getEmail(),
            'avatar' => $user->getAvatar()
        ]);

        Auth::login($user);
        return redirect('/');
    }
}