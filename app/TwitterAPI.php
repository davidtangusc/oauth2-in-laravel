<?php

namespace App;

use GuzzleHttp\Client;

class TwitterAPI {
  public function __construct($config)
  {
    $this->accessToken = $this->getAccessToken($config['consumer_key'], $config['consumer_secret']);
  }

  protected function getAccessToken($consumerKey, $consumerSecret)
  {
    $this->client = new Client([
      'base_uri' => 'https://api.twitter.com'
    ]);

    $authorization = base64_encode("$consumerKey:$consumerSecret");
    $response = $this->client->request('POST', '/oauth2/token', [
      'headers' => [
        'Authorization' => "Basic $authorization"
      ],
      'form_params' => [
        'grant_type' => 'client_credentials'
      ]
    ]);

    if ($response->getStatusCode() === 200) {
      return json_decode($response->getBody()->getContents())->access_token;
    } else {
      throw new Exception('Cannot get access token');
    }
  }

  public function get($path, $query)
  {
    $response = $this->client->request('GET', "1.1/$path.json", [
      'headers' => [
        'Authorization' => "Bearer $this->accessToken"
      ],
      'query' => $query
    ]);

    if ($response->getStatusCode() === 200) {
      return $response->getBody()->getContents();
    } else {
      throw new Exception('Request failed');
    }
  }
}